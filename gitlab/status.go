package gitlab

import (
	gitlab "github.com/xanzy/go-gitlab"
)

type StatusAPI interface {
	SetCommitStatus(pid interface{}, sha string, opt *gitlab.SetCommitStatusOptions, options ...gitlab.OptionFunc) (*gitlab.CommitStatus, *gitlab.Response, error)
}

type StatusOptions struct {
	ProjectID   interface{}
	Ref         string
	SHA         string
	PipelineURL string
}

func Running(statusApi StatusAPI, opts StatusOptions) (*gitlab.CommitStatus, error) {
	return setCommitStatus(statusApi, opts, gitlab.Running)
}

func Success(statusApi StatusAPI, opts StatusOptions) (*gitlab.CommitStatus, error) {
	return setCommitStatus(statusApi, opts, gitlab.Success)
}

func Failed(statusApi StatusAPI, opts StatusOptions) (*gitlab.CommitStatus, error) {
	return setCommitStatus(statusApi, opts, gitlab.Failed)
}

func setCommitStatus(statusApi StatusAPI, opts StatusOptions, state gitlab.BuildStateValue) (*gitlab.CommitStatus, error) {
	setOpts := setOptions(opts)
	setOpts.State = state
	cs, _, err := statusApi.SetCommitStatus(opts.ProjectID, opts.SHA, setOpts)
	return cs, err
}

func setOptions(opts StatusOptions) *gitlab.SetCommitStatusOptions {
	var commitRef *string
	if opts.Ref != "" {
		commitRef = &opts.Ref
	}
	var url *string
	if opts.PipelineURL != "" {
		url = &opts.PipelineURL
	}
	name := "Deploy pipeline"
	desc := "Deploy to Kubernetes in progress..."
	return &gitlab.SetCommitStatusOptions{
		Ref:         commitRef,
		Name:        &name,
		Description: &desc,
		TargetURL:   url,
	}
}
