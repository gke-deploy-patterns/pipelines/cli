package gitlab

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gke-deploy-patterns/pipelines/cli/gitlab/mocks"
)

func setupMock(pid interface{}, sha string) *mocks.StatusAPI {
	m := &mocks.StatusAPI{}
	m.On("SetCommitStatus", pid, sha, mock.Anything).Return(&gitlab.CommitStatus{}, &gitlab.Response{}, nil)
	return m
}

func TestStatus(t *testing.T) {
	tests := []struct {
		sa                          *mocks.StatusAPI
		opts                        StatusOptions
		name, description, ref, url string
	}{
		{
			setupMock(1, "abc123"),
			StatusOptions{
				ProjectID: 1,
				SHA:       "abc123",
			},
			"Deploy pipeline",
			"Deploy to Kubernetes in progress...",
			"",
			"",
		},
		{
			setupMock(12, "abc321"),
			StatusOptions{
				ProjectID: 12,
				SHA:       "abc321",
				Ref:       "feature/branch",
			},
			"Deploy pipeline",
			"Deploy to Kubernetes in progress...",
			"feature/branch",
			"",
		},
		{
			setupMock(12, "abc321"),
			StatusOptions{
				ProjectID:   12,
				SHA:         "abc321",
				Ref:         "feature/branch",
				PipelineURL: "http://url",
			},
			"Deploy pipeline",
			"Deploy to Kubernetes in progress...",
			"feature/branch",
			"http://url",
		},
	}

	for _, tt := range tests {
		optsEqual := func(opts *gitlab.SetCommitStatusOptions) {
			assert.Equal(t, tt.name, *opts.Name)
			assert.Equal(t, tt.description, *opts.Description)
			if tt.ref != "" || opts.Ref != nil {
				assert.Equal(t, tt.ref, *opts.Ref)
			}
			if tt.url != "" || opts.TargetURL != nil {
				assert.Equal(t, tt.url, *opts.TargetURL)
			}
		}

		Running(tt.sa, tt.opts)
		Success(tt.sa, tt.opts)
		Failed(tt.sa, tt.opts)
		tt.sa.AssertExpectations(t)
		opts := tt.sa.Calls[0].Arguments.Get(2).(*gitlab.SetCommitStatusOptions)
		assert.Equal(t, gitlab.Running, opts.State)
		optsEqual(opts)

		opts = tt.sa.Calls[1].Arguments.Get(2).(*gitlab.SetCommitStatusOptions)
		assert.Equal(t, gitlab.Success, opts.State)
		optsEqual(opts)

		opts = tt.sa.Calls[2].Arguments.Get(2).(*gitlab.SetCommitStatusOptions)
		assert.Equal(t, gitlab.Failed, opts.State)
		optsEqual(opts)
	}
}
