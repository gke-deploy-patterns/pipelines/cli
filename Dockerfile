FROM golang:1.10.3-alpine AS go
WORKDIR /go/src/gitlab.com/gke-deploy-patterns/pipelines/cli
COPY . .
RUN go test ./... \
 && CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/pipelinecli

FROM docker:git
RUN apk update && apk add --no-cache curl
COPY --from=go /usr/local/bin/pipelinecli /usr/local/bin/pipelinecli