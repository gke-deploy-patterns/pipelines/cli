package cmd

import (
	"testing"

	"github.com/stretchr/testify/mock"
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gke-deploy-patterns/pipelines/cli/gitlab/mocks"

	"github.com/stretchr/testify/assert"
)

func setupMock(pid, sha string) *mocks.StatusAPI {
	m := &mocks.StatusAPI{}
	m.On("SetCommitStatus", pid, sha, mock.Anything).Return(&gitlab.CommitStatus{}, &gitlab.Response{}, nil)
	return m
}

func TestStatus(t *testing.T) {
	tests := []struct {
		sa                            *mocks.StatusAPI
		project, state, ref, sha, url string
		err                           error
	}{
		{
			setupMock("1", "sha123"),
			"1",
			"running",
			"",
			"sha123",
			"",
			nil,
		},
		{
			setupMock("21", "sha123"),
			"21",
			"success",
			"master",
			"sha123",
			"",
			nil,
		},
		{
			setupMock("42", "sha123"),
			"42",
			"failed",
			"develop",
			"sha123",
			"http://url",
			nil,
		},
	}
	for _, tt := range tests {
		gitlabProject = tt.project
		ref = tt.ref
		sha = tt.sha
		pipelineURL = tt.url

		err := status(tt.sa, tt.state)
		tt.sa.AssertExpectations(t)
		assert.Equal(t, tt.err, err)

		opts := tt.sa.Calls[0].Arguments.Get(2).(*gitlab.SetCommitStatusOptions)
		assert.Equal(t, tt.state, string(opts.State))
		if tt.ref != "" || opts.Ref != nil {
			assert.Equal(t, tt.ref, *opts.Ref)
		}
		if tt.url != "" || opts.TargetURL != nil {
			assert.Equal(t, tt.url, *opts.TargetURL)
		}
	}
}
