package cmd

import (
	"fmt"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gitlab.com/gke-deploy-patterns/pipelines/cli/kube"
	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

var manifestFile string
var kinds []string
var namespace bool

var verifyCmd = &cobra.Command{
	Use:           "verify",
	Short:         "Run assertions on Kubernetes manifests",
	SilenceUsage:  true,
	SilenceErrors: true,
	RunE:          verify,
}

func init() {
	rootCmd.AddCommand(verifyCmd)
	verifyCmd.Flags().StringVarP(&manifestFile, "file", "f", "manifest.yml", "manifest file")
	verifyCmd.Flags().StringSliceVarP(&kinds, "kind", "k", []string{}, "allowed kind(s)")
	verifyCmd.Flags().BoolVar(&namespace, "namespace", false, "require namespace in metadata")
}

func verify(*cobra.Command, []string) error {
	obj, err := kube.Decode(manifestFile)
	if err != nil {
		return err
	}

	k := obj.GetObjectKind().GroupVersionKind().Kind
	if !validateKind(k, kinds) {
		return fmt.Errorf("object kind \"%s\" is %s", color.CyanString(k), color.RedString("not allowed"))
	}
	fmt.Printf("Object kind \"%s\" is %s\n", color.CyanString(k), color.GreenString("allowed"))

	if namespace {
		ns := obj.(v1.Object).GetNamespace()
		if ns == "" {
			return fmt.Errorf("Object namespace is %s", color.RedString("not provided"))
		}
		fmt.Printf("Object namespace is \"%s\"\n", color.CyanString(ns))
	}

	return nil
}

func validateKind(kind string, kinds []string) bool {
	if len(kinds) == 0 {
		return true
	}
	for _, k := range kinds {
		if k == kind {
			return true
		}
	}
	return false
}
