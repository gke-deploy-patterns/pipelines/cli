package cmd

import (
	"fmt"
	"os"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	gitlab_status "gitlab.com/gke-deploy-patterns/pipelines/cli/gitlab"
)

var gitlabToken, gitlabURL, gitlabProject, sha, ref, pipelineURL string

var statusCmd = &cobra.Command{
	Use:           "status (running|success|failed)",
	Short:         "Report pipeline status",
	SilenceUsage:  true,
	SilenceErrors: true,
	Args:          cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		gl := gitlab.NewClient(nil, gitlabToken)
		err := gl.SetBaseURL(gitlabURL)
		if err != nil {
			return err
		}

		return status(gl.Commits, args[0])
	},
}

func init() {
	rootCmd.AddCommand(statusCmd)
	statusCmd.Flags().StringVarP(&gitlabToken, "token", "t", "", "Gitlab token")
	statusCmd.Flags().StringVarP(&gitlabURL, "url", "u", "https://gitlab.com/api/v4", "Gitlab URL")
	statusCmd.Flags().StringVarP(&gitlabProject, "project", "p", "", "Gitlab project")
	statusCmd.Flags().StringVarP(&sha, "sha", "s", "", "Commit SHA")
	statusCmd.Flags().StringVarP(&ref, "ref", "r", "", "Git ref (branch or tag)")
	statusCmd.Flags().StringVar(&pipelineURL, "pipeline-url", "", "Link to pipeline details (default: $CI_PIPELINE_URL)")
}

func status(api gitlab_status.StatusAPI, state string) error {
	var url string
	if pipelineURL != "" {
		url = pipelineURL
	} else if u, ok := os.LookupEnv("CI_PIPELINE_URL"); ok {
		url = u
	}

	opts := gitlab_status.StatusOptions{
		ProjectID:   gitlabProject,
		Ref:         ref,
		SHA:         sha,
		PipelineURL: url,
	}

	var cs *gitlab.CommitStatus
	var err error
	switch state {
	case "success":
		cs, err = gitlab_status.Success(api, opts)
	case "failed":
		cs, err = gitlab_status.Failed(api, opts)
	default:
		cs, err = gitlab_status.Running(api, opts)
	}
	if err != nil {
		return err
	}

	fmt.Printf("Set commit status to %s\n", color.WhiteString("%+v", cs))
	return nil
}
