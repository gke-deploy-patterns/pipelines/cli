package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/spf13/cobra"

	"github.com/stretchr/testify/assert"
)

func TestVerify(t *testing.T) {
	tests := []struct {
		manifest  string
		kinds     []string
		namespace bool
		err       error
	}{
		{
			`apiVersion: v1
kind: Service
spec: {}`,
			[]string{"Deployment", "Service"},
			false,
			nil,
		},
		{
			`apiVersion: v1
kind: Service
spec: {}`,
			[]string{"Deployment"},
			false,
			fmt.Errorf("object kind \"Service\" is not allowed"),
		},
		{
			`apiVersion: v1
kind: Service
spec: {}`,
			[]string{"Service"},
			true,
			fmt.Errorf("Object namespace is not provided"),
		},
		{
			`apiVersion: v1
kind: Service
metadata:
  namespace: space
spec: {}`,
			[]string{"Service"},
			true,
			nil,
		},
	}
	for _, tt := range tests {
		f, _ := ioutil.TempFile("", "verify_test")
		defer os.Remove(f.Name())

		f.Write([]byte(tt.manifest))
		f.Close()
		manifestFile = f.Name()

		kinds = tt.kinds
		namespace = tt.namespace

		err := verify(&cobra.Command{}, []string{})
		assert.Equal(t, tt.err, err)
	}
}
