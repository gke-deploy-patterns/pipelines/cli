package kube

import (
	"io/ioutil"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
)

func Decode(path string) (runtime.Object, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	o, _, err := scheme.Codecs.UniversalDeserializer().Decode(b, nil, nil)
	return o, err
}
