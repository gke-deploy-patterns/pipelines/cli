package kube

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDecode(t *testing.T) {
	tests := []struct {
		manifest string
		kind     string
		err      error
	}{
		{
			`apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: demo
  namespace: default
  labels:
    app: demo
spec: {}`,
			"Deployment",
			nil,
		},
		{
			`apiVersion: v1
kind: Service
spec: {}`,
			"Service",
			nil,
		},
		{
			`malformed`,
			"",
			fmt.Errorf("couldn't get version/kind; json parse error: json: cannot unmarshal string into Go value of type struct { APIVersion string \"json:\\\"apiVersion,omitempty\\\"\"; Kind string \"json:\\\"kind,omitempty\\\"\" }"),
		},
	}
	for _, tt := range tests {
		f, _ := ioutil.TempFile("", "decode_test")
		defer os.Remove(f.Name())

		f.Write([]byte(tt.manifest))
		f.Close()

		obj, err := Decode(f.Name())
		if tt.err != nil {
			assert.Equal(t, tt.err, err)
		}
		if tt.kind != "" {
			assert.Equal(t, tt.kind, obj.GetObjectKind().GroupVersionKind().Kind)
		}
	}
}
