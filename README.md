# pipeline cli

Command Line Interface for GKE deployments. Performs Kubernetes validations and interacts with the Gitlab API.

```bash
go build -o pipelinecli
pipelinecli verify -f deployment.yml --kind Deployment --kind Service --namespace
```

This project publishes a Docker image `btimperman/pipelinecli` for ease of use in Gitlab pipelines:

```yaml
gitlab build job:
  image: btimperman/pipelinecli
  script: 
    - pipelinecli verify -f deployment.yml --kind Deployment
```