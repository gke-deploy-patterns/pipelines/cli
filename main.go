package main

import "gitlab.com/gke-deploy-patterns/pipelines/cli/cmd"

func main() {
	cmd.Execute()
}
